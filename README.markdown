Clubber
=======

Clubber is a Perl script to make forming `chroot` environments less of a task
that makes you want to cry and kill yourself. It requires `ldd`. It should be
run interactively as `root`, *never* as an automated or unattended task.

It includes all the `libnss*` libraries for you, since even static binaries
require these libraries for `libc` functions. It doesn't include files like
`/etc/passwd` or `/etc/resolv.conf`, though; those are up to you to craft.

If you're going to use Clubber to import your libraries for your `chroot`
environment, make sure you run it with `--dry` first, and that you're sure
you understand what it's going to do.

A list of the Perl modules required is at the top of the script; they're all
reasonably standard, and are probably on your system already.

Usage
-----

Run with one or more binaries as parameters and no other options, `clubber`
will run `ldd` over each, converge the list of libraries used by all of them,
and print them to `stdout`.

    # clubber /usr/bin/php

Run with the `--chroot=PATH` option, pointing to an existing directory intended
as the root of a `chroot` jail, `clubber` will instead create required library
paths if they do not exist and copy the libraries into that environment if they
do not yet exist, or if they differ from the host system libraries.

    # clubber --chroot=/chroot/apache /usr/bin/php

Run with both the `--chroot=PATH` and `--dry` options, `clubber` will perform a
"dry run" of the `chroot` library import, writing a summary of what it would do
on `stdout`.

    # clubber --chroot=/chroot/apache --dry /usr/bin/php

Caveats
-------

This only works for compile-time dynamic linking that `ldd` understands. Any
files a program might require to run that `ldd` wouldn't tell you about won't
get imported. A good example might be `/etc/resolv.conf` or `/etc/passwd`.

It's also up to you to make sure that the library paths that your program uses
in `chroot` will enable it to actually find all these libraries you're
importing from the host system directories.

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under an [MIT License][2].

[1]: https://sanctum.geek.nz/
[2]: https://www.opensource.org/licenses/MIT
